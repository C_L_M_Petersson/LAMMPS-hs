{-# LANGUAGE TupleSections #-}
module Examples.Advanced where

import           Control.Monad                              (when)
import           Control.Monad.Extra
import           Control.Parallel.MPI.Simple                hiding (Group)

import           Data.List                                  (isInfixOf,
                                                             elemIndex,sortOn)

import           LAMMPS.Advanced.Commands
import           LAMMPS.Advanced.Commands.Actions
import           LAMMPS.Advanced.Commands.Atom              as A
import           LAMMPS.Advanced.Commands.Atom.Elements     (Element (Au, Cr,
                                                             Fe, H, O))
import           LAMMPS.Advanced.Commands.Atom.Styles       as AS
import           LAMMPS.Advanced.Commands.Molecule
import           LAMMPS.Advanced.Commands.Molecule.Types
import           LAMMPS.Advanced.Commands.Box               as B
import           LAMMPS.Advanced.Commands.Dump              as D
import           LAMMPS.Advanced.Commands.Dump.Styles       as DS
import           LAMMPS.Advanced.Commands.Fix
import           LAMMPS.Advanced.Commands.Fix.Styles        as FS
import           LAMMPS.Advanced.Commands.ForceField
import           LAMMPS.Advanced.Commands.ForceField.Styles
import           LAMMPS.Advanced.Commands.Initialisation
import           LAMMPS.Advanced.Commands.Settings
import           LAMMPS.Advanced.Commands.Thermo
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils

import           System.Directory
import           System.Posix.Process

import           Tools.Advanced.Query


lmpMolecule :: LAMMPS()
lmpMolecule = do

    header "Initialise"
    atoms <- initialise
    let [h,o,cr] = atoms
        h2  = Molecule (AB  2)         [h,h]
        o2  = Molecule (AB  2)         [o,o]
        h2o = Molecule (A2B 4 1.82387) [h,h,o]


    header "Simulation Box"

    lattice (fcc uc (1,0,0) (0,1,0) (0,0,1))
    rBeam <- region $ Block "beam" (-20) (-0) 0 w 0 w

    newLine
    command "create_atoms" ["3","region","beam"]

    placeMolecules h2  (0.00,0.00,0.00) (3,3,3)
    placeMolecules o2  (4.00,0.00,0.00) (3,3,3)
    placeMolecules h2o (4.00,4.00,4.00) (3,3,3)

    newLine

    header "Atoms"

    setupReaxFF "files/ffield.reax.CrOFeSCH" atoms "NULL" []

    header "Execution"

    timestep dt
    withDump dumpAll "dump.all" 10
        $ withFixes [fixNVE,fixTemp,fixQEQ]
        $ run 10000 []

    where
        uc =  4.00
        w  = 22.00
        dt =  0.25

        dumpAll = Dump All [DS.Atom] []

        fixNVE  = Fix All [NVE] []
        fixTemp = Fix All [FS.Temp,Berendsen] ["500","500","100"]
        fixQEQ  = reaxFFQEQFix All 1 0 10 1e-6

        placeMolecules :: Molecule -> Coordinate -> (Int,Int,Int) -> LAMMPS()
        placeMolecules mol displace ns = mapM_ (placeMolecule mol) xyzs
            where
                (nx,ny,nz) = let (nx',ny',nz') = ns in ( fromIntegral nx'
                                                       , fromIntegral ny'
                                                       , fromIntegral nz')

                xmin = 2.5
                xmax = 20-xmin
                ymin = 0.25
                ymax = w-xmin
                zmin = ymin
                zmax = ymax

                xyzs = concat $ concat
                    [[[ displace + (x,y,z)
                        | i <- [0..nx-1] :: [Bound]
                        , let x  = xmin + (xmax-xmin)*i/(nx-1) ]
                        | j <- [0..nx-1] :: [Bound]
                        , let y  = ymin + (ymax-ymin)*j/(ny-1) ]
                        | k <- [0..nz-1] :: [Bound]
                        , let z  = zmin + (zmax-zmin)*k/(nz-1) ]


        initialise :: LAMMPS [Atom]
        initialise = do

            units Real
            atomStyle Charge

            boundary P P P
            region (Block "box" (-20) 20 0 w 0 w) >>= createBox 3
            defineAtoms [H,O,Cr]


        header :: String -> LAMMPS()
        header x = let ds     = length x`replicate`'-'
                       filler = ("--"++) . (++"--")
                    in newLines 2 >> comments (map filler [ds,x,ds]) >> newLine


lmpOxidation :: LAMMPS()
lmpOxidation = do

    header "Initialise"
    atoms <- initialise
    let [o,cr] = atoms

    header "Simulation Box"

    newLine
    createLatticeAtoms (bcc     uc  (1,0,0) (0,1,0) (0,0,1)) [[cr]]
                       (Block "" 0 wx 0 wy (-zMetal/2)      ( zMetal/2))
                       1 (uc/4,uc/4,uc/4) 0
    createLatticeAtoms (sc (2.0*uc) (1,0,0) (0,1,0) (0,0,1)) [[o]]
                       (Block "" 0 wx 0 wy ( zMetal/2+zGap) ( zMax-zGap))
                       1 0 0
    createLatticeAtoms (sc (2.0*uc) (1,0,0) (0,1,0) (0,0,1)) [[o]]
                       (Block "" 0 wx 0 wy (-zMax+zGap)     (-zMetal/2-zGap))
                       1 0 0

    gMetal <- group "gMetal" =<< region
            (Block "rMetal" INF INF INF INF (-zMetal-zGap/2) ( zMetal+zGap/2))
    gGas   <- A.subtract "gGas" All [gMetal]

    newLine

    header "Atoms"

    setupReaxFF "files/ffield.reax.CrOFeSCH" atoms "NULL" []

    header "Execution"

    writeDump dumpAll "dump.all"

    timestep dt
    withDump dumpAll "dump.all" 10
        $ withFixes [fixNPTMetal gMetal,fixNPTGas gGas,fixQEQ]
        $ run 10000 []

    where
        uc     =  2.88
        wx     =  8*uc
        wy     =  8*uc
        zMax   =  40
        zGap   =   5
        zMetal =   7.5

        dt   =   1

        dumpAll = Dump All [DS.Atom] []

        fixNPTMetal gGas = Fix gGas [NPT] ["temp", "300", "300","100.0"
                                          ,"x"   ,   "0",   "0","100.0"
                                          ,"y"   ,   "0",   "0","100.0"
                                          ]
        fixNPTGas   gGas = Fix gGas [NPT] ["temp", "300", "300","100.0"
                                          ,"z"   ,"1000","1000","100.0"
                                          ]
        fixQEQ           = reaxFFQEQFix All 1 0 10 1e-6

        initialise :: LAMMPS [Atom]
        initialise = do

            units Real
            atomStyle Charge

            boundary P P P
            lattice (bcc uc (1,0,0) (0,1,0) (0,0,1))
            region (Block "box" 0 wx 0 wy (-zMax) zMax) >>= createBox 2
            defineAtoms [O,Cr]


        header :: String -> LAMMPS()
        header x = let ds     = length x`replicate`'-'
                       filler = ("--"++) . (++"--")
                    in newLines 2 >> comments (map filler [ds,x,ds]) >> newLine


lmpLowPressureOxidation :: LAMMPS()
lmpLowPressureOxidation = do

    header "Initialise"
    atoms <- initialise
    let [o,fe] = atoms

    header "Simulation Box"

    newLine
    createLatticeAtoms (bcc     uc  (1,0,0) (0,1,0) (0,0,1)) [[fe]]
                       (Block "" 0 wx 0 wy (-zMetal/2)      ( zMetal/2))
                       1 (uc/4,uc/4,uc/4) 0

    rMetal <- region $ Block "rMetal" INF INF INF INF INF INF
    gMetal <- group "gMetal" rMetal
    delete rMetal

    newLine

    header "Atoms"

    setupReaxFF "files/ffield.reax.CrOFeSCH" atoms "NULL" []

    header "Minimisation"
    withDump dumpAll "dump.all" 1
        . withFixes [fixQEQ]
        $ command "minimize" ["0","1.0e-16","1000","10000"]

    header "Execution"

    writeDump dumpAll "dump.all"

    timestep dt
    withDump dumpAll "dump.all" 10 $ runNSteps o fe gMetal 0
    where
        uc         =  2.88::Bound
        wx         = 10*uc::Bound
        wy         = 10*uc::Bound
        zMax       = 25
        zVel       = 10.0
        zMetal     =  7.5

        temp       = 1000 --[K]
        press      =   10 --[Atm.]

        bondOO     = 1.2
        bondOMetal = (uc+bondOO)/2

        nRelax     =  10000
        nTot       = 100000

        dt         = 1

        dumpAll = Dump All [DS.Atom] []

        fixNPT         = Fix All [NPT] ["temp", show temp, show temp, "100"
                                       ,"x"   ,       "0",       "0", "100"
                                       ,"y"   ,       "0",       "0", "100"
                                       ]
        fixBerendsen g = Fix g [FS.Temp,Berendsen] ["300","300","1000"]
        fixQEQ         = reaxFFQEQFix All 1 0 10 1e-6

        initialise :: LAMMPS [Atom]
        initialise = do

            units Real
            atomStyle Charge

            boundary P P P
            lattice (bcc uc (1,0,0) (0,1,0) (0,0,1))
            region (Block "box" 0 wx 0 wy (-zMax) zMax) >>= createBox 2
            defineAtoms [O,Fe]


        header :: String -> LAMMPS()
        header x = let ds     = length x`replicate`'-'
                       filler = ("--"++) . (++"--")
                    in newLines 2 >> comments (map filler [ds,x,ds]) >> newLine


        runNSteps :: Atom -> Atom -> Group -> Int -> LAMMPS()
        runNSteps aOxygen aMetal gMetal n
            | n>=nTot   = return()
            | otherwise = do
                gGas      <- getGasGroup gMetal n

                nO2Curr   <- (`div`2)<$>getDumpNAtoms gGas
                nO2Target <- targetNO2s gMetal
                let nO2Target = nO2Curr

                placeO2s aOxygen n nO2Curr nO2Target
                updateVelocities n

                [fixNPT,fixQEQ,fixBerendsen gGas]`withFixes`run nRelax []

                runNSteps aOxygen aMetal gMetal (n+nRelax)
            where
                fixNPT         = Fix All [NPT] ["temp",show temp,show temp,"100.0"
                                               ,"x","0","0","100.0"
                                               ,"y","0","0","100.0"]
                fixQEQ         = reaxFFQEQFix All 1 0 10 1e-6
                fixBerendsen g = Fix g [FS.Temp,Berendsen]
                                       [show temp,show temp,"1000"]

        updateVelocities :: Int -> LAMMPS()
        updateVelocities n = do
                rBoundL <- region $ Block "rLargeBoundL" INF INF
                                                         INF INF
                                                         INF zVel
                rBoundH <- region $ Block "rLargeBoundH" INF INF
                                                         INF INF
                                                         (-zVel) INF
                rBound  <- region $ Union "rLargeBound"  [rBoundL,rBoundH]

                gBound  <- group "gLargeBound" rBound
                mapM_ delete [rBoundL,rBoundH,rBound]

                velocity gBound (AS.Create ((\(Bound x) -> x) temp) (n+1))
                                [Rot Yes,Mom Yes]
                command "group" ["gLargeBound","delete"]

        placeO2s :: Atom -> Int -> Int -> Int -> LAMMPS()
        placeO2s aOxygen n nO2Curr nO2Target
            | nO2Curr>=nO2Target = return()
            | otherwise          = do

                gBound <- getGBound n
                xyzs   <- getProcessedDumpAtomInfo (\[x,y,z] -> (x,y,z))
                                                            gBound ["x","y","z"]

                placeO2sRec n aOxygen xyzs (nO2Target-nO2Curr) True
                                $ potentialO2XYs xyzs
            where
                getGNewVel :: Int -> LAMMPS Group
                getGNewVel n = do
                    rBoundL <- region $ Block "rBoundL" INF INF
                                                        INF INF
                                                        INF (-zMax+2.5*bondOO)
                    rBoundH <- region $ Block "rBoundH" INF INF
                                                        INF INF
                                                        (zMax-2.5*bondOO) INF
                    rBound  <- region $ Union "rBound"  [rBoundL,rBoundH]

                    when (n/=0) $ command "group" ["gNewVel","delete"]
                    gNewVel  <- group "gNewVel" rBound

                    mapM_ delete [rBoundL,rBoundH,rBound]

                    return gNewVel


                getGBound :: Int -> LAMMPS Group
                getGBound n = do
                    rBoundL <- region $ Block "rBoundL" INF INF
                                                        INF INF
                                                        INF (-zMax+2.5*bondOO)
                    rBoundH <- region $ Block "rBoundH" INF INF
                                                        INF INF
                                                        (zMax-2.5*bondOO) INF
                    rBound  <- region $ Union "rBound"  [rBoundL,rBoundH]

                    when (n/=0) $ command "group" ["gBound","delete"]
                    gBound  <- group "gBound" rBound

                    mapM_ delete [rBoundL,rBoundH,rBound]

                    return gBound


                placeO2sRec :: Int -> Atom -> [Coordinate] -> Int -> Bool
                                                -> [(Bound,Bound)] -> LAMMPS()
                placeO2sRec ti _       _  _ _     [] = return ()
                placeO2sRec ti aOxygen cs n above ((x,y):xys)
                    | n<1                            = return ()
                    | not $ null nearby              = placeO2sRec ti aOxygen cs n above xys
                    | otherwise                      = do
                        let zs = if above then [ zMax-3*bondOO/2, zMax-  bondOO/2]
                                          else [-zMax+  bondOO/2,-zMax+3*bondOO/2]

                        mapM_ ((aOxygen`createSingleAtom`) . (x,y,)) zs

                        placeO2sRec ti aOxygen ((x,y,0):cs) (n-1) (not above) xys
                    where nearby = let cutOff = 2*bondOO
                                    in filter (\(x',y',_) -> abs(x-x')<cutOff
                                                          && abs(y-y')<cutOff) cs

                placeO2 :: Atom -> [Coordinate] -> Bound -> Bound -> LAMMPS()
                placeO2 aOxygen xyzs x y
                    | null nearby = mapM_ ((aOxygen`createSingleAtom`) . (x,y,))
                                          [zMax-bondOO/2,-zMax+bondOO/2]
                    | otherwise   = return()
                    where nearby = let cutOff = 2*bondOO
                                    in filter (\(x',y',_) -> abs(x-x')<cutOff
                                                          && abs(y-y')<cutOff) xyzs

                potentialO2XYs :: [Coordinate] -> [(Bound,Bound)]
                potentialO2XYs cs = potentialO2XYsRec cs $ map (,0) rawXYs
                    where
                        rawXYs = let raws min max = takeWhile (<max)
                                                  $ map ((+min) . (*(2*bondOO))) [0..]
                                 in [ (x,y) | x <- 0`raws`wx
                                            , y <- 0`raws`wy ]

                        potentialO2XYsRec :: [Coordinate] -> [((Bound,Bound),Bound)]
                                                          -> [(Bound,Bound)]
                        potentialO2XYsRec _  []   = []
                        potentialO2XYsRec cs xyds = (x,y)
                                                  : potentialO2XYsRec [(x,y,0)] xyds'
                            where (((x,y),_):xyds') = sortOn snd
                                                    $ map (updateDistSum cs) xyds


                        updateDistSum :: [Coordinate] -> ((Bound,Bound),Bound)
                                                      -> ((Bound,Bound),Bound)
                        updateDistSum cs ((x,y),d) = ((x,y),d+distSum cs x y)

                        withDistSum :: [Coordinate] -> (Bound,Bound) -> ((Bound,Bound),Bound)
                        withDistSum cs (x,y) = ((x,y),distSum cs x y)

                        distSum :: [Coordinate] -> Bound -> Bound -> Bound
                        distSum cs x y = sum $ map (dist x y) cs

                        dist :: Bound -> Bound -> Coordinate -> Bound
                        dist x y (x',y',_) = 1/( d2 x x' 0 wx
                                               + d2 y y' 0 wy
                                               )
                            where d2 p p' min max = let width = max-min
                                                     in minimum $ map ((**2) . (+p))
                                                            [-p',-p'-width,-p'+width]

        targetNO2s :: Group -> LAMMPS Int
        targetNO2s gMetal = do
            --PV = nRT => n = PV/RT, [R] = [PV/nT]
            --  P [atmospheres]
            --  V [Angstroms^3 = 10^{-30}m^3]
            -- =
            --  n [n]
            --  R [atmospheres * Angstroms^3 / ( number * Kelvin ) ]
            --  T [K]
            (zMetalMin,zMetalMax) <- getGroupZLimits gMetal
            let volume = wx * wy * ( 2*zMax-(zMetalMax-zMetalMin) )
                idealGasConstant = 136.2594621268195 -- R [Ang^3 atm/K]

            return . round $ press*volume/(idealGasConstant*temp)


        getGasGroup :: Group -> Int -> LAMMPS Group
        getGasGroup gMetal n = do
            (zMetalMin,zMetalMax) <- getGroupZLimits gMetal

            rGasL <- region $ Block "rGasL" INF INF
                                            INF INF
                                            INF (zMetalMin-bondOMetal)
            rGasH <- region $ Block "rGasH" INF INF
                                            INF INF
                                            (zMetalMax+bondOMetal) INF
            rGas  <- region $ Union "rGas"  [rGasL,rGasH]

            when (n/=0) $ command "group" ["gGas","delete"]
            gGas  <- group "gGas" rGas

            mapM_ delete [rGasL,rGasH,rGas]

            return gGas

        getGroupZLimits :: Group -> LAMMPS (Bound,Bound)
        getGroupZLimits g = (\zs -> (minimum zs,maximum zs)) . map head
                                <$>getDumpAtomInfo g ["z"]
