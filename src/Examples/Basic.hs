module Examples.Basic where

import           LAMMPS.Basic.Commands
import           LAMMPS.Basic.Commands.Actions
import           LAMMPS.Basic.Commands.Atom              as A
import           LAMMPS.Basic.Commands.Atom.Styles       as AS
import           LAMMPS.Basic.Commands.Box               as B
import           LAMMPS.Basic.Commands.Dump              as D
import           LAMMPS.Basic.Commands.Dump.Styles       as DS
import           LAMMPS.Basic.Commands.Fix
import           LAMMPS.Basic.Commands.Fix.Styles
import           LAMMPS.Basic.Commands.ForceField
import           LAMMPS.Basic.Commands.ForceField.Styles as FFS
import           LAMMPS.Basic.Commands.Initialisation
import           LAMMPS.Basic.Commands.Settings
import           LAMMPS.Basic.Commands.Thermo            as T
import           LAMMPS.Basic.State
import           LAMMPS.Basic.Utils


lmpNanoWire :: LAMMPS()
lmpNanoWire = do

    newLines 2
    comments [ "----------------"
             , "-- Initialise --"
             , "----------------"
             ]
    newLine

    initialise

    newLines 2
    comments [ "--------------------"
             , "-- Simulation Box --"
             , "--------------------"
             ]
    newLine

    rBeam <- region $ Block "beam" (0*uc) (100*uc) 0 w 0 w

    lattice (B.FCC uc) []
    createBox 1 rBeam

    createRegionAtoms a1 rBeam


    let bGroup id xl xr = region (Block id xl xr INF INF INF INF) >>= group id
    gLBound <- bGroup "lBound" ( 0*uc) (  3*uc)
    gRBound <- bGroup "rBound" (97*uc) (100*uc)

    gBound <- "gBound"`union`[gLBound,gRBound]

    newLines 2
    comments [ "-----------"
             , "-- Atoms --"
             , "-----------"
             ]
    newLine

    mass Wildcard m

    newLine
    pairStyle [FFS.LJ,Cut] ["7.23"]
    pairCoeff Wildcard Wildcard ["0.1515","2.338"]

    newLine
    velocity All (AS.Create 0.01 511124) [Rot Yes,Mom Yes]

    newLine
    timestep dt

    newLine
    thermo 100
    thermoStyle $ T.Custom ["step","vol","temp"]

    newLines 2
    comments [ "---------------"
             , "-- Execution --"
             , "---------------"
             ]
    newLine

    dump dump1 500 [D.Append Yes]
    mapM_ fix [fix1,fix2]

    run 5000 []

    unDump dump1
    mapM_ unFix [fix1,fix2]

    where
        uc = 3.615
        w  = 6*uc
        m  = 63.546
        dt = 0.001

        a1 = A.Atom "1"

        fix1 = Fix "1" All [NVT] ["temp","0.01","0.01","0.01"]
        fix2 = Fix "2" All [SetForce] ["0","0","0"]

        dump1 = Dump "1" All [DS.Atom] "dump.all" []

        initialise :: LAMMPS()
        initialise = do
            units Metal
            dimension 3
            atomStyle Atomic
            boundary S S S
