module LAMMPS.Core.Commands.Settings where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.State



timestep :: LAMMPSState s => Double -> LAMMPS s ()
timestep dt = command "timestep" [show dt]
