module LAMMPS.Core.Commands.Dump.Styles where



data DumpStyle = Atom     | CFG      | Custom   | DCD      | GZ
               | H5MD     | Image    | Local    | MolFile  | Movie
               | MPIIO    | NetCDF   | VTK      | XTC      | XYZ
               | XYZMPIIO | Zstd
    deriving(Eq,Show)
