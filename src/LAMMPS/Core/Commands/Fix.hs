module LAMMPS.Core.Commands.Fix
(   Fix(..)

,   fix
,   unFix
) where

import           Data.List

import           LAMMPS.Core.Commands
import           LAMMPS.Core.Commands.Atom
import           LAMMPS.Core.Commands.Fix.Styles
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils


data Fix = Fix
           { lID :: ID
           , g   :: Group
           , fss :: [FixStyle]
           , as  :: [Argument]
           }


instance LAMMPSObject Fix where
    getID = lID

instance Show Fix where
    show (Fix lID g fss as) = unwords $
        [ lID, getID g, intercalate "/" $ map showLower fss ]++as


fix :: LAMMPSState s => Fix -> LAMMPS s Fix
fix f = command "fix" [show f] >> return f

unFix :: LAMMPSState s => Fix -> LAMMPS s ()
unFix = command "unfix" . (:[]) . getID
