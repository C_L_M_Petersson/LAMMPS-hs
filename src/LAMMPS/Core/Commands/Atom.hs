module LAMMPS.Core.Commands.Atom where

import qualified LAMMPS.Core.Commands.Atom.Styles as AS
import           LAMMPS.Core.Commands
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils


data Group = All | Group { groupID :: ID }
    deriving(Eq)

instance Show Group where
    show = getID

instance LAMMPSObject Group where
    getID g
        | g == All  = "all"
        | otherwise = groupID g


atomStyle :: LAMMPSState s => AS.AtomStyle -> LAMMPS s AS.AtomStyle
atomStyle a = command "atom_style" [showLower a] >> return a


createRegionAtoms :: (LAMMPSState s,LAMMPSObject a,LAMMPSObject r) => a -> r ->
                     LAMMPS s ()
createRegionAtoms a r = command "create_atoms" [getID a,"region",getID r]

createRandomAtoms :: (LAMMPSState s,LAMMPSObject a,LAMMPSObject r) => a -> r ->
                     Int -> Seed -> LAMMPS s ()
createRandomAtoms a r n s = generate s >>= command "create_atoms" .
    ([getID a,"random",show n]++) . (:[getID r]) . show

createSingleAtom :: (LAMMPSState s,LAMMPSObject a) => a -> Bound -> Bound ->
                    Bound -> LAMMPS s ()
createSingleAtom a x y z = command "create_atoms"
                         $ getID a:"single":map show [x,y,z]++["units","box"]


group :: (LAMMPSState s,LAMMPSObject r) => ID -> r -> LAMMPS s Group
group lID r = command "group" [lID,"region",getID r] >> return (Group lID)

union :: LAMMPSState s => ID -> [Group] -> LAMMPS s Group
union lID gs = command "group" ([lID,"union"]++map getID gs)
    >> return (Group lID)

subtract :: LAMMPSState s => ID -> Group -> [Group] -> LAMMPS s Group
subtract lID g gs = command "group" (lID:"subtract":map getID (g:gs))
    >> return (Group lID)


mass :: LAMMPSState s => Range -> Double -> LAMMPS s ()
mass r m = command "mass" [show r,show m]


velocity :: LAMMPSState s => Group -> AS.VelocityStyle -> [AS.VelocityKeyword]
            -> LAMMPS s ()
velocity g vs vk = command "velocity"
                 $ getID g:show vs:map show vk++["units","box"]


readData :: LAMMPSState s => FilePath -> Argument -> [Argument] -> LAMMPS s ()
readData fp kw = command "read_data" . (fp:) . (kw:)

writeData :: LAMMPSState s => FilePath -> [Argument] -> LAMMPS s ()
writeData fp = command "write_data" . (fp:)
