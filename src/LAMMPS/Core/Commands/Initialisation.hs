module LAMMPS.Core.Commands.Initialisation where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils



data Units = LJ | Metal | Real | SI | CGS | Electron | Micro | Nano
    deriving(Eq,Show)

units :: LAMMPSState s => Units -> LAMMPS s ()
units us = command "units" [showLower us]
