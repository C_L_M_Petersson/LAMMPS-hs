module LAMMPS.Core.Commands.Thermo where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils



thermo :: LAMMPSState s => Int -> LAMMPS s ()
thermo n = command "thermo" [show n]



data ThermoStyle = One | Multi | Custom { as :: [Argument] }
    deriving(Eq)

instance Show ThermoStyle where
    show  One        = "one"
    show  Multi      = "multi"
    show (Custom as) = unwords $ "custom":as

thermoStyle :: LAMMPSState s => ThermoStyle -> LAMMPS s ThermoStyle
thermoStyle ts = command "thermo_style" (words $ show ts) >> return ts
