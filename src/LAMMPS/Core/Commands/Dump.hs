module LAMMPS.Core.Commands.Dump where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.Commands.Atom
import           LAMMPS.Core.Commands.Dump.Styles
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils


data Dump = Dump
            { lID :: ID
            , g   :: Group
            , ds  :: [DumpStyle]
            , fp  :: FilePath
            , as  :: [Argument]
            }

instance LAMMPSObject Dump where
    getID = lID


newtype DumpModification = Append
                           { yn :: YN
                           }

instance Show DumpModification where
    show (Append yn) = "append "++show yn


dump :: LAMMPSState s => Dump -> Int -> LAMMPS s Dump
dump d n = command "dump" ([lID,getID g,unSlashLower ds,show n,fp]++as)
        >> return d
    where Dump lID g ds fp as = d

unDump :: LAMMPSState s => Dump -> LAMMPS s ()
unDump = command "undump" . (:[]) . getID


writeDump :: LAMMPSState s => Dump -> LAMMPS s Dump
writeDump d = command "write_dump" ([getID g,unSlashLower ds,fp]++as)
                >> return d
    where Dump _ g ds fp as = d


dumpModify :: LAMMPSState s => Dump -> DumpModification -> LAMMPS s Dump
dumpModify d dm = unvoid (command "dump_modify" . (:[show dm]) . getID) d


readDump :: LAMMPSState s => FilePath -> Int -> [Argument] -> LAMMPS s ()
readDump fp i = command "read_dump" . (fp:) . (show i:)
