module LAMMPS.Core.Commands.Actions where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils



data RunKeyword = UpTo
                | Start { i  :: Int }
                | Stop  { i  :: Int }
                | Pre   { yn :: YN  }
                | Post  { yn :: YN  }
                -- | Every <- hanteras bättre av haskell? Gäller detta för andra också?
    deriving(Eq)

instance Show RunKeyword where
    show  UpTo      = "upto"
    show (Start  i) = "start "++show i
    show (Stop   i) = "stop " ++show i
    show (Pre   yn) = "pre "  ++show yn
    show (Post  yn) = "post " ++show yn



run :: LAMMPSState s => Int -> [RunKeyword] -> LAMMPS s ()
run i = command "run" . (show i:) . map show
