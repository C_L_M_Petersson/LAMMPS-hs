module LAMMPS.Core.Commands.ForceField.Styles where



data PairStyle = ADP            | AGNI           | AIREBO
               | Alloy          | Atm            | AWPMD
               | Beck           | BODY           | BOP
               | Born           | Brownian       | Buck
               | Buck6d         | C              | CD
               | Charmm         | CHARMMFSH      | CHARMMFSW
               | Class2         | Coaxstk        | Colloid
               | Comb           | COMB3          | Cosine
               | Coul           | CoulGauss      | Crespi
               | CS             | Cubic          | Cut
               | Debye          | Density        | DH
               | Diel           | Dipole         | DMI
               | DPD            | Dreiding       | DRIP
               | DSF            | DSMC           | E3B
               | EAM            | EDIP           | EDPD
               | EFF            | EIM            | Energy
               | EPS            | Exchange       | EXCV
               | Exp6           | Expand         | EXTEP
               | FDT            | FS             | Full
               | Gauss          | GayBerne       | Gran
               | Granular       | Graphene       | Gromacs
               | GW             | Harmonic       | HBN
               | Hbond          | Heatconduction | Hertz
               | History        | Histoy         | Hooke
               | Hybrid         | Idealgas       | ILP
               | Implicit       | Isothermal     | KIM
               | Kolmogorov     | LCBOP          | Lebedeva
               | Lennard        | Line           | Linear
               | List           | LJ             | LJ96
               | Local          | Long           | LPS
               | Lubricate      | Lubricateu     | Lucy
               | Magelec        | MDF            | MDPD
               | Meam           | MesoCNT        | Mesont
               | MGPt           | Mie            | MLIAP
               | MM3            | Mod            | MOMB
               | Morris         | Morse          | MSM
               | Multi          | NB3B           | Neel
               | NM             | None           | NParticle
               | Old            | Overlay        | OXDNA
               | Oxdna2         | OXRNA2         | Peri
               | PMB            | Poly           | Polygon
               | Polyhedron     | Polymorphic    | Python
               | Quip           | R              | Reax
               | Rebo           | Resquared      | Rhosum
               | Rounded        | RX             | SDK
               | Sdpd           | SF             | Shield
               | Slater         | SMD            | Smooth
               | Smtbq          | Snap           | Soft
               | Sph            | Spin           | Spline
               | Squared        | SRP            | STK
               | Streitz        | SW             | Switch3
               | Table          | Taitwater      | TDPD
               | Tersoff        | Thole          | TIP4P
               | Tlsph          | TPM            | Tri
               | Tri_surface    | TSTAT          | TT
               | Ufm            | ULSPH          | Vashishta
               | Ves            | Wolf           | XSTK
               | Yukawa         | Z              | ZBL
               | Zero
    deriving(Eq,Show)
