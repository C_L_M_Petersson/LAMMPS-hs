module LAMMPS.Core.Commands.Atom.Styles where

import           LAMMPS.Core.Utils


data AtomStyle = Angle     | Atomic    | Body      | Bond      | Charge
               | Dipole    | Dpd       | Edpd      | Mdpd      | Tdpd
               | Electron  | Ellipsoid | Full      | Line      | Meso
               | Molecular | Peri      | Smd       | Sphere    | Spin
               | Tri       | Template  | Hybrid
    deriving(Eq,Show)


data VelocityStyle = Create
                     { temp :: Double
                     , seed :: Int
                     }
                   | Set
                     { vx :: Double
                     , vy :: Double
                     , vz :: Double
                     }
                   | Scale
                     { temp :: Double
                     }
                   -- | Ramp
                   | ZeroLinear
                   | ZeroAngular

instance Show VelocityStyle where
    show (Create t s)   = unwords ["create",show t,show s]
    show (Set vx vy vz) = unwords ("set":map show [vx,vy,vz])
    show (Scale t)      = "scale "++show t
    show ZeroLinear     = "zero linear"
    show ZeroAngular    = "zero angular"


data VelocityKeyword = DistUniform
                     | DistGaussian
                     | Mom  YN
                     | Rot  YN
                     | Temp ID
                     | Bias YN
                     | LoopAll
                     | LoopLocal
                     | LoopGeom
                     -- | Rigid Fix

instance Show VelocityKeyword where
    show DistUniform  = "dist uniform"
    show DistGaussian = "dist gaussian"
    show (Mom  yn)    = "mom " ++show yn
    show (Rot  yn)    = "rot " ++show yn
    show (Temp lID)   = "temp "++lID
    show (Bias yn)    = "bias "++show yn
    show LoopAll      = "loop all"
    show LoopLocal    = "loop local"
    show LoopGeom     = "loop geom"
