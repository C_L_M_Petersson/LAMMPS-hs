{-# LANGUAGE GADTs #-}
module LAMMPS.Core.Commands.Box
(   createBox

,   Bound(..)

,   BoundaryCondition(..)
,   boundary

,   Lattice(..)
,   Orientation(X,Y,Z,Orient)
,   orientationDir
,   invertOrientation
,   lattice
,   latticeConstant
,   isLatticeType
,   toLatticeType

,   Region(..)
,   region
,   delete

,   dimension
) where

import           LAMMPS.Core.Commands
import           LAMMPS.Core.Commands.Atom
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils


createBox :: LAMMPSState s => Int -> Region -> LAMMPS s Region
createBox n r = command "create_box" [show n,getID r] >> return r

modifyBox :: LAMMPSState s => Group -> LAMMPS s ()
modifyBox g = return()


data BoundaryCondition = S  | P  | F  | M
                       | SP | PS | FS | MS
                       | SF | PF | FP | MP
                       | SM | PM | FM | MF
    deriving(Eq,Show)


boundary :: LAMMPSState s => BoundaryCondition -> BoundaryCondition ->
            BoundaryCondition -> LAMMPS s ()
boundary x y z = command "boundary" $ map showLower [x,y,z]


data Lattice a where
    None :: Num a => { latticeConstant :: a } -> Lattice a
    BCC  :: Num a => { latticeConstant :: a } -> Lattice a
    FCC  :: Num a => { latticeConstant :: a } -> Lattice a
    SC   :: Num a => { latticeConstant :: a } -> Lattice a

instance Eq a => Eq (Lattice a) where
    None lC==None lC' = lC==lC'
    BCC  lC==BCC  lC' = lC==lC'
    FCC  lC==FCC  lC' = lC==lC'
    SC   lC==SC   lC' = lC==lC'
    _      ==_        = False

instance (Show a) => Show (Lattice a) where
    show (None lC) = "none "++show lC
    show (BCC  lC) = "bcc " ++show lC
    show (FCC  lC) = "fcc " ++show lC
    show (SC   lC) = "sc  " ++show lC


isLatticeType :: Eq a => Lattice a -> (a -> Lattice a) -> Bool
isLatticeType l lt = l==l`toLatticeType`lt

toLatticeType :: Lattice a -> (a -> Lattice a) -> Lattice a
toLatticeType l lt = lt $ latticeConstant l


data Orientation = X | Y | Z
                 | Orient
                   { dim  :: Char
                   , xDir :: Int
                   , yDir :: Int
                   , zDir :: Int
                   }
    deriving(Eq)

instance Show Orientation where
    show (Orient dim x y z) = unwords $ "orient":[dim]:map show [x,y,z]
    show  o                 = show $ explicit o

explicit :: Orientation -> Orientation
explicit X = Orient 'x' 1 0 0
explicit Y = Orient 'y' 0 1 0
explicit Z = Orient 'z' 0 0 1
explicit o = o

orientationDir :: Orientation -> (Bound,Bound,Bound)
orientationDir (Orient _ xi yi zi) = (x,y,z)
    where
        x   = fromIntegral xi/len
        y   = fromIntegral yi/len
        z   = fromIntegral zi/len
        len = sqrt $ fromIntegral(xi^2+yi^2+zi^2)
orientationDir  o                  = orientationDir $ explicit o

invertOrientation :: Orientation -> Orientation
invertOrientation o
    | o==X||o==Y||o==Z = invertOrientation $ explicit o
    | otherwise        = o{ xDir = -xDir o, yDir = -yDir o, zDir = -zDir o  }


lattice :: (LAMMPSState s,Show a) => Lattice a -> [Orientation] -> LAMMPS s ()
lattice l os = command "lattice" (show l:map show os)


data Region = Block
              { lID :: ID
              , xlo :: Bound, xhi :: Bound
              , ylo :: Bound, yhi :: Bound
              , zlo :: Bound, zhi :: Bound
              }
            | Sphere
              { lID :: ID
              , x   :: Bound
              , y   :: Bound
              , z   :: Bound
              , r   :: Bound
              }
            -- | Cone
            -- | Cylinder
            -- | Plane
            -- | Prism
            | Union
              { lID :: ID
              , rs  :: [Region]
              }
    deriving(Eq)

instance LAMMPSObject Region where
    getID = lID

instance Show Region where
    show (Block  lID xlo xhi ylo yhi zlo zhi) = unwords $ [lID,"block"]
                                              ++map show
                                                    [xlo,xhi,ylo,yhi,zlo,zhi]
    show (Sphere lID x y z r                ) = unwords $ [lID,"sphere"]
                                              ++map show
                                                     [x,y,z,r]
    show (Union  lID rs                     ) = unwords
                                              $ [lID,"union",show $ length rs]
                                              ++map getID rs


region :: LAMMPSState s => Region -> LAMMPS s Region
region = unvoid (command "region" . (:["units","box"]) . show)

delete :: LAMMPSState s => Region -> LAMMPS s ()
delete r = command "region" [getID r,"delete"]


dimension :: LAMMPSState s => Int -> LAMMPS s Int
dimension = unvoid (command "dimension" . (:[]) . show)
