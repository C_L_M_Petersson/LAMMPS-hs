module LAMMPS.Core.Commands.ForceField where

import           Data.Char
import           Data.List

import           LAMMPS.Core.Commands
import           LAMMPS.Core.Commands.Atom
import           LAMMPS.Core.Commands.ForceField.Styles
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils



pairStyle :: LAMMPSState s => [PairStyle] -> [Argument] -> LAMMPS s ()
pairStyle ps as = command "pair_style" (unSlashLower ps:as)

pairCoeff :: LAMMPSState s => Range -> Range -> [Argument] -> LAMMPS s ()
pairCoeff i j as = command "pair_coeff" (show i:show j:as)
