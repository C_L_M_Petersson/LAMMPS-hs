{-# LANGUAGE CApiFFI                  #-}
{-# LANGUAGE ForeignFunctionInterface #-}
module LAMMPS.Core.FFI
(   Ptr

,   lammpsOpenNoMPI
,   lammpsOpen
,   lammpsClose

,   lammpsFile
,   lammpsCommand
) where

import           Control.Monad
import           Control.Parallel.MPI.Internal
import           Control.Parallel.MPI.Simple

import           Foreign
import           Foreign.C.String
import           Foreign.C.Types



lammpsOpenNoMPI :: IO(Ptr())
lammpsOpenNoMPI = c_lammps_open_no_mpi 0 nullPtr nullPtr

foreign import ccall "lammps_open_no_mpi"
     c_lammps_open_no_mpi :: CInt -> Ptr CString -> Ptr(Ptr()) -> IO(Ptr())


lammpsOpen :: IO(Ptr())
lammpsOpen = c_lammps_open 0 nullPtr c_mpi_comm_world nullPtr

foreign import ccall "lammps_open"
     c_lammps_open :: CInt -> Ptr CString -> Ptr() -> Ptr(Ptr()) -> IO(Ptr())


lammpsClose :: Ptr() -> IO()
lammpsClose = c_lammps_close

foreign import ccall "lammps_close"
     c_lammps_close :: Ptr() -> IO()



lammpsFile :: Ptr() -> String -> IO()
lammpsFile p = c_lammps_file p<=<newCString

foreign import ccall "lammps_file"
     c_lammps_file :: Ptr() -> CString -> IO()


lammpsCommand :: Ptr() -> String -> IO()
lammpsCommand p = c_lammps_command p<=<newCString

foreign import ccall "lammps_command"
     c_lammps_command :: Ptr() -> CString -> IO()



foreign import capi "mpi.h value MPI_COMM_WORLD"
    c_mpi_comm_world :: Ptr()
