{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
module LAMMPS.Core.Utils
(   LAMMPSObject
,   getID
,   ID

,   Bound(..)

,   YN(..)

,   Argument

,   showLower
,   unSlashLower

,   Range(..)

,   Seed(..)
,   generate

,   Coordinate
,   (<|>)
,   (><)
,   norm
,   norm2
,   normalise
,   cmap
,   dirCoordinate

,   unvoid
) where

import           Control.Monad.IO.Class

import           Data.Char
import           Data.Data
import           Data.List

import           Numeric.Limits

import           System.Random


type ID = String

class LAMMPSObject a where
    getID :: a -> ID


data Bound = Bound Double | INF | EDGE
    deriving(Data,Eq)

instance Enum Bound where
    toEnum   = Bound . toEnum
    fromEnum (Bound d) = fromEnum d
    fromEnum INF       = fromEnum maxValue
    fromEnum EDGE      = fromEnum maxValue

instance Floating Bound where
    pi                          = Bound pi
    exp      (Bound x)          = Bound $ exp x
    exp       INF               = INF
    exp       EDGE              = EDGE
    log      (Bound x)          = Bound $ log x
    log       INF               = INF
    log       EDGE              = EDGE
    sin      (Bound x)          = Bound $ sin x
    sin       INF               = INF
    sin       EDGE              = EDGE
    cos      (Bound x)          = Bound $ cos x
    cos       INF               = INF
    cos       EDGE              = EDGE
    asin     (Bound x)          = Bound $ asin x
    asin      INF               = INF
    asin      EDGE              = EDGE
    acos     (Bound x)          = Bound $ acos x
    acos      INF               = INF
    acos      EDGE              = EDGE
    atan     (Bound x)          = Bound $ atan x
    atan      INF               = INF
    atan      EDGE              = EDGE
    sinh     (Bound x)          = Bound $ sinh x
    sinh      INF               = INF
    sinh      EDGE              = EDGE
    cosh     (Bound x)          = Bound $ cosh x
    cosh      INF               = INF
    cosh      EDGE              = EDGE
    asinh    (Bound x)          = Bound $ asinh x
    asinh     INF               = INF
    asinh     EDGE              = EDGE
    acosh    (Bound x)          = Bound $ acosh x
    acosh     INF               = INF
    acosh     EDGE              = EDGE
    atanh    (Bound x)          = Bound $ atanh x
    atanh     INF               = INF
    atanh     EDGE              = EDGE
    (**)    (Bound x) (Bound y) = Bound (x**y)
    (**)     INF       _        = INF
    (**)     EDGE      _        = EDGE
    logBase (Bound x) (Bound y) = Bound (logBase x y)
    logBase  INF       _        = INF
    logBase  EDGE      _        = EDGE

instance Fractional Bound where
    Bound x/Bound y = Bound (x/y)
    INF    /_       = INF
    _      /INF     = 0
    EDGE   /_       = EDGE
    _      /EDGE    = 0
    fromRational    = Bound . fromRational

instance Num Bound where
    Bound x+Bound y  = Bound (x+y)
    INF    +_        = INF
    _      +INF      = INF
    EDGE   +_        = EDGE
    _      +EDGE     = EDGE
    Bound x*Bound y  = Bound (x*y)
    INF    *_        = INF
    _      *INF      = INF
    EDGE   *_        = EDGE
    _      *EDGE     = EDGE
    abs (Bound x)    = Bound (abs x)
    abs INF          = INF
    abs EDGE         = EDGE
    negate (Bound x) = Bound (negate x)
    negate INF       = INF
    negate EDGE      = EDGE
    signum (Bound x) = Bound (signum x)
    signum INF       = Bound 1
    signum EDGE      = Bound 1
    fromInteger      = Bound . fromInteger

instance Ord Bound where
    Bound x<=Bound y= x<=y
    INF    <=_      = False
    _      <=INF    = True
    EDGE   <=_      = False
    _      <=EDGE   = True

instance Random Bound where
    randomR (Bound x,Bound y) g = let (z,g') = randomR (x,y) g
                                   in (Bound z,g')
    randomR (INF    ,Bound x) g = let (y,g') = randomR (minValue,x) g
                                   in (Bound y,g')
    randomR (Bound x,INF    ) g = let (y,g') = randomR (x,minValue) g
                                   in (Bound y,g')
    randomR (EDGE   ,b      ) g = randomR (INF,b  ) g
    randomR (b      ,EDGE   ) g = randomR (b  ,INF) g
    random g = let (x,g') = random g
                in (Bound x,g')

instance Read Bound where
    readsPrec _ "EDGE" = [(EDGE          ,"")]
    readsPrec _ "INF"  = [(INF           ,"")]
    readsPrec _ x      = [(Bound (read x),"")]

instance Real Bound where
    toRational  EDGE     = error "toRational EDGE not defined"
    toRational  INF      = error "toRational INF not defined"
    toRational (Bound x) = toRational x

instance RealFrac Bound where
    properFraction  EDGE     = error "properFraction EDGE not defined"
    properFraction  INF      = error "properFraction INF not defined"
    properFraction (Bound x) = let (n,f) = properFraction x
                                in (n,Bound f)

instance Show Bound where
    show (Bound x) = show x
    show INF       = "INF"
    show EDGE      = "EDGE"


data YN = Yes | No deriving(Eq)

instance Show YN where
    show Yes = "yes"
    show No  = "no"


type Argument = String


showLower :: Show a => a -> Argument
showLower = map toLower . show

unSlashLower :: Show a => [a] -> Argument
unSlashLower = intercalate "/" . map showLower


data Range = Value  { x   :: Int }
           | Min    { xlo :: Int }
           | Max    { xhi :: Int }
           | MinMax { xlo :: Int, xhi :: Int }
           | Wildcard
    deriving(Eq)

instance Show Range where
    show (Value  x      ) = show x
    show (Min    xlo    ) = show xlo++"*"
    show (Max    xhi    ) = "*"++show xhi
    show (MinMax xlo xhi) = show xlo++"*"++show xhi
    show  Wildcard        = "*"


data Seed = Random | Seed Int

instance Show Seed where
    show (Seed i) = show i

instance Num Seed where
    Seed x+Seed y = seed (x+y)
    _     +_      = Random
    Seed x*Seed y = seed (x*y)
    _     *_      = Random
    Seed x-Seed y = seed (x-y)
    _     -_      = Random
    abs           = id
    negate        = id
    signum _      = Seed 1
    fromInteger   = seed . fromInteger


seed :: Int -> Seed
seed = Seed . (`mod`maxSeed) . (+1) . abs
    where maxSeed = 1000000000

generate :: MonadIO m => Seed -> m Seed
generate Random = liftIO $ seed <$> randomIO
generate s      = return s


type Coordinate = (Bound,Bound,Bound)

instance Fractional Coordinate where
    (x,y,z)/(x',y',z') = (x/x',y/y',z/z')
    fromRational r  = let d = fromRational r in (d,d,d)

instance Num Coordinate where
    (x,y,z)+(x',y',z') = (x+x',y+y',z+z')
    (x,y,z)*(x',y',z') = (x*x',y*y',z*z')
    abs    (x,y,z)     = let a = sqrt(x**2+y**2+z**2) in (a,a,a)
    negate (x,y,z)     = (-x,-y,-z)
    signum c           = c/abs c
    fromInteger i      = let d = fromInteger i in (d,d,d)

cmap :: (Bound -> Bound) -> Coordinate -> Coordinate
cmap f (x,y,z) = (f x,f y,f z)

(<|>) :: Coordinate -> Coordinate -> Bound
(x,y,z)<|>(x',y',z') = x*x' + y*y' + z*z'

(><) :: Coordinate -> Coordinate -> Coordinate
(x,y,z)><(x',y',z') = (y*z' - z*y', z*x' - x*z', x*y' - y*x')

norm :: Coordinate -> Bound
norm = sqrt . norm2

norm2 :: Coordinate -> Bound
norm2 c = c<|>c

normalise :: Coordinate -> Coordinate
normalise c = (/norm c)`cmap`c

dirCoordinate :: Bound -> Bound -> Coordinate
dirCoordinate theta phi = ( sin theta*cos phi
                          , sin theta*sin phi
                          , cos theta
                          )


unvoid :: Monad m => (a -> m b) -> a -> m a
unvoid f x = f x >> return x
