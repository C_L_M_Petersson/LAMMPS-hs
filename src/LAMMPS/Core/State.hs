{-# LANGUAGE ScopedTypeVariables #-}
module LAMMPS.Core.State
(   LAMMPS
,   LAMMPSState(..)
,   LAMMPSContext(..)
,   LAMMPSPipe

,   withLAMMPS

,   getNCPUs
,   getRank
,   getPipe
,   isRoot
,   liftIO

,   byLAMMPSPipe

,   captureLAMMPS
,   captureLAMMPS_
) where

import           Control.Monad.Extra
import           Control.Monad.State
import           Control.Parallel.MPI.Simple

import           Data.Bool.HT
import           Data.ByteString.Char8       (unpack)

import           LAMMPS.Core.FFI

import           System.IO
import           System.IO.Silently
import           System.Posix.Redirect


type LAMMPS s a = StateT s IO a

class LAMMPSState s where
    nCPUs :: s -> Int
    rank  :: s -> Int
    pipe  :: s -> LAMMPSPipe

data LAMMPSContext = MultipleContexts [LAMMPSContext]
                   | Echo
                   | Execute
                   | Write FilePath

data LAMMPSPipe = MultiplePipes [LAMMPSPipe]
                | EchoPipe
                | ExecutePipe (Ptr())
                | WritePipe Handle


withLAMMPS :: LAMMPSState s => (Int -> Int -> LAMMPSPipe -> s) -> LAMMPSContext
              -> LAMMPS s a -> IO()
withLAMMPS s lmpC lmp = withMPI $ \n r ->
    filterMIO error (openLAMMPS s lmpC n r) >>= execStateT lmp
                                            >>= filterMIO error . closeLAMMPS
    where
        withMPI :: (Int -> Int -> IO()) -> IO()
        withMPI f = mpiWorld $ \n r -> f n (fromIntegral r)

openLAMMPS :: LAMMPSState s => (Int -> Int -> LAMMPSPipe -> s) -> LAMMPSContext
              -> Int -> Int -> IO s
openLAMMPS s lmpC n r = s n r<$>case lmpC of
    MultipleContexts lmpCs -> MultiplePipes<$>mapM openLAMMPS' lmpCs
    lmpC                   -> openLAMMPS' lmpC
    where
        openLAMMPS' :: LAMMPSContext -> IO LAMMPSPipe
        openLAMMPS'  Echo      = return EchoPipe
        openLAMMPS'  Execute   = ExecutePipe <$> silence lammpsOpen
        openLAMMPS' (Write fp) = WritePipe   <$> openFile fp WriteMode

closeLAMMPS :: LAMMPSState s => s -> IO()
closeLAMMPS = (barrier commWorld>>) . caseLAMMPSPipe (return())
                                                     lammpsClose
                                                     hClose . pipe


getNCPUs :: LAMMPSState s => LAMMPS s Int
getNCPUs = gets nCPUs

getRank :: LAMMPSState s => LAMMPS s Int
getRank = gets rank

getPipe :: LAMMPSState s => LAMMPS s LAMMPSPipe
getPipe = gets pipe

isRoot :: LAMMPSState s => LAMMPS s Bool
isRoot = (==0) <$> getRank


byLAMMPSPipe :: LAMMPSState s => IO() -> (Ptr() -> IO()) -> (Handle -> IO()) ->
                LAMMPS s ()
byLAMMPSPipe f g h = getPipe >>= caseLAMMPSPipe (isRoot`whenM`liftIO f)
                                                (liftIO . g)
                                                (whenM isRoot . liftIO . h)

caseLAMMPSPipe :: forall m. Monad m => m() -> (Ptr() -> m()) -> (Handle -> m())
                    -> LAMMPSPipe -> m()
caseLAMMPSPipe fEcho fExecute fWrite lmpP = case lmpP of
    MultiplePipes lmpPs -> mapM_ caseLAMMPSPipe' lmpPs
    lmpP'               -> caseLAMMPSPipe' lmpP'
    where
        caseLAMMPSPipe' :: LAMMPSPipe -> m()
        caseLAMMPSPipe'  EchoPipe         = fEcho
        caseLAMMPSPipe' (ExecutePipe ptr) = fExecute ptr
        caseLAMMPSPipe' (WritePipe   h' ) = fWrite   h'


captureLAMMPS :: LAMMPSState s => IO a -> LAMMPS s String
captureLAMMPS = captureMIO errorLAMMPS

captureLAMMPS_ :: LAMMPSState s => IO a -> LAMMPS s ()
captureLAMMPS_ = captureMIO_ errorLAMMPS

errorLAMMPS :: LAMMPSState s => String -> LAMMPS s a
errorLAMMPS str = get >>= liftIO . closeLAMMPS >> error ("LAMMPS "++str)


captureMIO :: MonadIO m => (String -> m String) -> IO a -> m String
captureMIO e x = liftIO (unpack . fst <$> redirectStdout x) >>=
        \s -> if take 5 s=="ERROR" then e s else return s

captureMIO_ :: MonadIO m => (String -> m String) -> IO a -> m()
captureMIO_ e = void . captureMIO e

filterMIO :: MonadIO m => (String -> m String) -> IO a -> m a
filterMIO e x = liftIO (redirectStdout x) >>= \(s,y) ->
    captureMIO_ e (print $ unpack s) >> return y
