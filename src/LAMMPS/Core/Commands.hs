module LAMMPS.Core.Commands
(   file
,   command

,   comment
,   comments

,   newLine
,   newLines
) where

import           Control.Monad

import           Data.Char
import           Data.List

import           LAMMPS.Core.FFI
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils

import           System.IO
import           System.IO.Silently


file :: LAMMPSState s => FilePath -> LAMMPS s ()
file fp = byLAMMPSPipe (readFile fp>>=putStrLn)
                       (silence . (readFile fp>>=) . lammpsFile)
                       ((readFile fp>>=) . hPutStr)

command :: LAMMPSState s => Argument -> [Argument] -> LAMMPS s ()
command c as = byLAMMPSPipe
                    (putStrLn (unwords $ c:as))
                    (silence . flip lammpsCommand (unwords $ c:as))
                    (`hPutStr`(dropWhileEnd isSpace (unwords (c:as))++"\n"))


comment :: LAMMPSState s => String -> LAMMPS s ()
comment = flip command [] . ('#':)

comments :: LAMMPSState s => [String] -> LAMMPS s ()
comments = mapM_ comment


newLine :: LAMMPSState s => LAMMPS s ()
newLine = command "" []

newLines :: LAMMPSState s => Int -> LAMMPS s ()
newLines = (`replicateM_`newLine)
