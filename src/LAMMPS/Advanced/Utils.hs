module LAMMPS.Advanced.Utils
(   module LAMMPS.Core.Utils

,   Plane(XY,XZ,YZ,Plane)
,   point
,   normal
,   vectors

,   head'
,   uncurry3
) where

import           LAMMPS.Core.Utils

import           System.Random


data Plane = XY | YZ | XZ
           | Plane
             { planePoint :: Coordinate
             , planeVec0  :: Coordinate
             , planeVec1  :: Coordinate
             }
    deriving(Show)

explicit :: Plane -> Plane
explicit XY = Plane 0 (1,0,0) (0,1,0)
explicit XZ = Plane 0 (1,0,0) (0,0,1)
explicit YZ = Plane 0 (0,1,0) (0,0,1)
explicit p  = p

point :: Plane -> Coordinate
point (Plane p _ _) = p
point  p            = point $ explicit p

normal :: Plane -> Coordinate
normal (Plane _ v0 v1) = normalise(v0><v1)
normal  p              = normal $ explicit p

vectors :: Plane -> (Coordinate,Coordinate)
vectors (Plane _ v0 v1) = (v0,v1)
vectors  p              = vectors $ explicit p


head' :: String -> [a] -> a
head' _   (x:_) = x
head' err []    = error err

uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (x,y,z) = f x y z
