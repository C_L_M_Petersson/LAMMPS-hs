module LAMMPS.Advanced.Commands.Molecule
(   Molecule(..)

,   placeMolecule
) where

import           Data.List

import           LAMMPS.Advanced.Commands.Atom
import           LAMMPS.Advanced.Commands.Molecule.Types
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils


data Molecule = Molecule
                { moleculeType :: MoleculeType
                , atoms        :: [Atom]
                }

instance Eq Molecule where
    m==m' = sort (map element $ atoms m)==sort (map element $ atoms m')

instance Show Molecule where
    show = concatMap (\(x:xs) -> show x++show (length (x:xs)))
         . Data.List.group . sort . map element . atoms


placeMolecule :: Molecule -> Coordinate -> LAMMPS Molecule
placeMolecule m c = zip (atoms m) . moleculeTypeDistribution
    (moleculeType m) c (map element $ atoms m)<$>getRandoms
        >>= mapM_ (uncurry createSingleAtom) >> return m
