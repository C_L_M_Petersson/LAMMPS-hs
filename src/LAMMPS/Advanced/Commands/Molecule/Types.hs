module LAMMPS.Advanced.Commands.Molecule.Types where

import           LAMMPS.Advanced.Commands.Atom.Elements
import           LAMMPS.Advanced.Utils


data MoleculeType = AB
                  { scale0 :: Bound
                  }
                  | A2B
                  { scale0 :: Bound
                  , ang0   :: Bound
                  }
    deriving(Eq)


moleculeTypeDistribution :: MoleculeType -> Coordinate -> [Element] -> [Bound]
                            -> [Coordinate]
moleculeTypeDistribution (AB scale0) c es angs = [c+fromCA,c-fromCB]
    where
        [eA,eB] = take 2 es
        [mA,mB] = map (Bound . atomicMass  ) [eA,eB]
        [rA,rB] = map (Bound . atomicRadius) [eA,eB]

        dist    = scale0*(rA*weightA + rB*weightB)

        [theta,phi] = take 2 angs
        dir         = dirCoordinate theta phi

        weightA = mA/(mA+mB)
        weightB = mB/(mA+mB)

        fromCA  = cmap (*(dist*weightA)) dir
        fromCB  = cmap (*(dist*weightA)) dir
moleculeTypeDistribution (A2B scale0 ang0) c es angs = [ c+fromCB+fromBA1
                                                        , c+fromCB+fromBA2
                                                        , c+fromCB]
    where
        [eA,eB] = take 2 es
        [mA,mB] = map (Bound . atomicMass  ) [eA,eB]
        [rA,rB] = map (Bound . atomicRadius) [eA,eB]

        dist    = scale0*(rA*weightA + rB*weightB)

        [alpha,theta,phi] = take 3 angs
        dirB              = dirCoordinate theta         phi
        dirAsep           = dirCoordinate (theta+pi/2) (phi+alpha)

        weightA = mA/(mA+mB)
        weightB = mB/(mA+mB)

        fromCB  = cmap (*(dist*cos(ang0/2)*2*mA/(2*mA+mB))) dirB
        fromBA1 = cmap (*(dist*cos(ang0/2)))                dirB
                + cmap (*(dist*sin(ang0/2)))                dirAsep
        fromBA2 = cmap (*(dist*cos(ang0/2)))                dirB
                - cmap (*(dist*sin(ang0/2)))                dirAsep
