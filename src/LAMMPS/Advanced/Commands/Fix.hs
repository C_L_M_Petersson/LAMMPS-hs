module LAMMPS.Advanced.Commands.Fix
(   Fix(..)

,   fix
,   unFix
,   withFix
,   withFixes
) where

import           Data.List

import           LAMMPS.Advanced.Commands.Fix.Styles
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.Atom
import qualified LAMMPS.Core.Commands.Fix            as Core


data Fix = Fix
           { g   :: Group
           , fss :: [FixStyle]
           , as  :: [Argument]
           }


toCoreFix :: Fix -> ID -> Core.Fix
toCoreFix f id = Core.Fix id (g f) (fss f) (as f)


fix :: Fix -> LAMMPS Fix
fix f = addFix (toCoreFix f) >>= Core.fix >> return f

unFix :: Fix -> LAMMPS ()
unFix f = remFix (toCoreFix f) >>= Core.unFix . toCoreFix f

withFix :: Fix -> LAMMPS a -> LAMMPS a
withFix f = (fix f>>) . (>>=((unFix f>>) . return))

withFixes :: [Fix] -> LAMMPS a -> LAMMPS a
withFixes = flip (foldr withFix)
