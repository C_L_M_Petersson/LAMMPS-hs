module LAMMPS.Advanced.Commands.Atom
(   module LAMMPS.Core.Commands.Atom
,   Atom(..)

,   defineAtoms
,   defineAtom

,   createSingleAtom
,   createLatticeAtoms
) where

import           Control.Monad
import           Control.Monad.IO.Class

import           Data.List
import           Data.Maybe

import           LAMMPS.Advanced.Commands
import           LAMMPS.Advanced.Commands.Atom.Elements (Atomic, Element,
                                                         atomicMass,
                                                         getElement)
import           LAMMPS.Advanced.Commands.Box
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.Atom              hiding
                                                        (createSingleAtom)
import qualified LAMMPS.Core.Commands.Atom              as Core


data Atom = Atom
            { atomID  :: ID
            , element :: Element
            }
    deriving(Eq,Show)

instance Atomic Atom where
    getElement = element

instance LAMMPSObject Atom where
    getID = atomID


defineAtoms :: [Element] -> LAMMPS [Atom]
defineAtoms = zipWithM defineAtom [1..]

defineAtom :: Int -> Element -> LAMMPS Atom
defineAtom i e = command "mass" [show $ Value i,show $ atomicMass a]>>return a
    where a = Atom (show i) e


createSingleAtom :: Atom -> Coordinate -> LAMMPS()
createSingleAtom a (x,y,z) = Core.createSingleAtom a x y z

createLatticeAtoms :: Lattice Bound -> [[Atom]] -> Region -> Coordinate
                        -> Coordinate -> Coordinate -> LAMMPS()
createLatticeAtoms l ass r ijkScale xyzShift ijkShift
    | l`isLatticeType`SC  = mapM_ (uncurry createSingleAtom) . zip (atom 1)
                            $ gridInRegion r (x0,y0,z0) oI iLC oJ jLC oK kLC
    | l`isLatticeType`BCC = createLatticeAtoms (l`toLatticeType`SC) [atom 1] r
                              ijkScale xyzShift  ijkShift
                         >> createLatticeAtoms (l`toLatticeType`SC) [atom 2] r
                              ijkScale xyzShift (ijkShift + (iLC/2,jLC/2,kLC/2))
    | l`isLatticeType`FCC = createLatticeAtoms (l`toLatticeType`SC) [atom 1] r
                              ijkScale xyzShift  ijkShift
                         >> createLatticeAtoms (l`toLatticeType`SC) [atom 2] r
                              ijkScale xyzShift (ijkShift + (0    ,jLC/2,kLC/2))
                         >> createLatticeAtoms (l`toLatticeType`SC) [atom 2] r
                              ijkScale xyzShift (ijkShift + (iLC/2,0    ,kLC/2))
                         >> createLatticeAtoms (l`toLatticeType`SC) [atom 2] r
                              ijkScale xyzShift (ijkShift + (iLC/2,jLC/2,0    ))
    where
        oI  = orientationX l
        oJ  = orientationY l
        oK  = orientationZ l

        iLC = iScale*latticeConstant l
        jLC = jScale*latticeConstant l
        kLC = kScale*latticeConstant l
        (iScale ,jScale ,kScale ) = ijkScale

        x0 = xShift+xShift'+minRegionDirValue X YZ r
        y0 = yShift+yShift'+minRegionDirValue Y XZ r
        z0 = zShift+zShift'+minRegionDirValue Z XY r
        (xShift ,yShift ,zShift ) = xyzShift
        (xShift',yShift',zShift') = let (iShift,jShift,kShift) = ijkShift
                                        (xi,yi,zi) = orientationDir oI
                                        (xj,yj,zj) = orientationDir oJ
                                        (xk,yk,zk) = orientationDir oK
                                     in ( xi*iShift + xj*jShift + xk*kShift
                                        , yi*iShift + yj*jShift + yk*kShift
                                        , zi*iShift + zj*jShift + zk*kShift
                                        )

        atom = cycle . (cycle ass!!)


addData :: FilePath -> LAMMPS ()
addData fp = readData fp "add" ["append"]
