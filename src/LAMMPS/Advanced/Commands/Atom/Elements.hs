module LAMMPS.Advanced.Commands.Atom.Elements
(   Element(..)

,   Atomic
,   getElement

,   atomicMass
,   atomicNumber
,   atomicRadius
,   electroNegativity
,   selfColoumbPotential
,   valenceOrbitalExponent
) where



data Element = H  | He | Li | Be | B  | C  | N  | O  | F  | Ne | Na | Mg | Al
             | Si | P  | S  | Cl | Ar | K  | Ca | Sc | Ti | V  | Cr | Mn | Fe
             | Co | Ni | Cu | Zn | Ga | Ge | As | Se | Br | Kr | Rb | Sr | Y
             | Zr | Nb | Mo | Tc | Ru | Rh | Pd | Ag | Cd | In | Sn | Sb | Te
             | I  | Xe | Cs | Ba | La | Ce | Pr | Nd | Pm | Sm | Eu | Gd | Tb
             | Dy | Ho | Er | Tm | Yb | Lu | Hf | Ta | W  | Re | Os | Ir | Pt
             | Au | Hg | Tl | Pb | Bi | Po | At | Rn
    deriving(Eq,Ord,Read,Show)



class Atomic a where
    getElement :: a -> Element

instance Atomic Element where
    getElement = id


atomicMass             :: Atomic a => a -> Double
atomicMass             = atomicMass'             . getElement

atomicNumber           :: Atomic a => a -> Int
atomicNumber           = atomicNumber'           . getElement

atomicRadius           :: Atomic a => a -> Double
atomicRadius           = atomicRadius'           . getElement

electroNegativity      :: Atomic a => a -> Double
electroNegativity      = electroNegativity'      . getElement

selfColoumbPotential   :: Atomic a => a -> Double
selfColoumbPotential   = selfColoumbPotential'   . getElement

valenceOrbitalExponent :: Atomic a => a -> Double
valenceOrbitalExponent = valenceOrbitalExponent' . getElement


atomicMass' :: Element -> Double
atomicMass' H  =   1.0080
atomicMass' C  =  12.0096
atomicMass' O  =  15.9994
atomicMass' S  =  32.0600
atomicMass' Cr =  51.9961
atomicMass' Fe =  55.8450
atomicMass' Pt = 195.084
atomicMass' Au = 196.9665
atomicMass' e  = error' "mass" e

atomicNumber' :: Element -> Int
atomicNumber' H  =  1
atomicNumber' C  =  6
atomicNumber' O  =  8
atomicNumber' S  = 16
atomicNumber' Cr = 24
atomicNumber' Fe = 26
atomicNumber' Pt = 78
atomicNumber' Au = 79
atomicNumber' e  = error' "atomic number" e

atomicRadius' :: Element -> Double
atomicRadius' H  = 0.25
atomicRadius' C  = 0.70
atomicRadius' O  = 0.60
atomicRadius' S  = 1.00
atomicRadius' Cr = 1.40
atomicRadius' Fe = 1.40
atomicRadius' Au = 1.35
atomicRadius' e  = error' "mass" e

electroNegativity' :: Element -> Double
electroNegativity' H  = 3.7248
electroNegativity' C  = 5.9666
electroNegativity' O  = 8.5000
electroNegativity' S  = 6.5745
electroNegativity' Cr = 1.4546
electroNegativity' Fe = 1.7785
electroNegativity' Au = 6.0000
electroNegativity' e  = error' "electronegativity" e

selfColoumbPotential' :: Element -> Double --Defined as in the QEQ
selfColoumbPotential' H  = 19.2186
selfColoumbPotential' C  = 14.0000
selfColoumbPotential' O  = 16.6244
selfColoumbPotential' S  = 18.0000
selfColoumbPotential' Cr = 17.9000
selfColoumbPotential' Fe = 17.2562
selfColoumbPotential' Au = 11.2962
selfColoumbPotential' e  = error' "electronegativity" e

valenceOrbitalExponent' :: Element -> Double
valenceOrbitalExponent' H  = 0.8203
valenceOrbitalExponent' C  = 0.9000
valenceOrbitalExponent' O  = 1.0898
valenceOrbitalExponent' S  = 0.7590
valenceOrbitalExponent' Cr = 0.5639
valenceOrbitalExponent' Fe = 0.4744
valenceOrbitalExponent' Au = 0.9623
valenceOrbitalExponent' e  = error' "electronegativity" e


error' :: String -> Element -> a
error' str e = error $ "The "++str++" is not defined for element "++show e++"."
