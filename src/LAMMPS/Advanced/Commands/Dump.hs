module LAMMPS.Advanced.Commands.Dump
(   Dump(..)
,   toCoreDump

,   DumpModification(..)

,   dump
,   unDump
,   dumpModify
,   withDump
,   withDumps

,   writeDump

,   readDump
) where

import           LAMMPS.Advanced.Commands.Atom
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.Dump        (DumpModification (..)
                                                    ,readDump)
import qualified LAMMPS.Core.Commands.Dump        as Core
import           LAMMPS.Core.Commands.Dump.Styles


data Dump = Dump
            { g  :: Group
            , ds :: [DumpStyle]
            , as :: [Argument]
            }

toCoreDump :: Dump -> FilePath -> ID -> Core.Dump
toCoreDump d fp id = Core.Dump id (g d) (ds d) fp (as d)


dump :: Dump -> FilePath -> Int -> LAMMPS Dump
dump d fp n = addDump (toCoreDump d fp) >>= flip Core.dump n >> return d

unDump :: Dump -> LAMMPS ()
unDump d = remDump (toCoreDump d "") >>= Core.unDump . toCoreDump d ""

dumpModify :: Dump -> DumpModification -> LAMMPS Dump
dumpModify d dm = let d' = toCoreDump d ""
                   in getDumpID d' >>= flip Core.dumpModify dm . d' >> return d

withDump :: Dump -> FilePath -> Int -> LAMMPS a -> LAMMPS a
withDump d fp n = (dump d fp n>>)
                . ((d`dumpModify`Append Yes)>>)
                . (>>=((unDump d>>) . return))

withDumps :: [(Dump,FilePath,Int)] -> LAMMPS a -> LAMMPS a
withDumps = flip (foldr (uncurry3 withDump))


writeDump :: Dump -> FilePath -> LAMMPS Dump
writeDump d fp = Core.writeDump (toCoreDump d fp "") >> return d
