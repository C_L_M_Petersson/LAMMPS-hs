{-# LANGUAGE GADTs #-}
module LAMMPS.Advanced.Commands.Box
(   module LAMMPS.Core.Commands.Box

,   Lattice(..), none,bcc,fcc,sc
,   lattice
,   latticeConstant
,   isLatticeType
,   toLatticeType

,   minRegionDirValue
,   maxRegionDirValue

,   gridInRegion
) where
import System.IO.Unsafe

import           Data.List
import           Data.Tuple.Extra

import           LAMMPS.Advanced.Commands
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.Box hiding (Lattice, isLatticeType,
                                           lattice, latticeConstant,
                                           toLatticeType)
import qualified LAMMPS.Core.Commands.Box as C


data Lattice a where
    Lattice :: Num a => { latticeStructure :: C.Lattice a
                        , orientationX     :: Orientation
                        , orientationY     :: Orientation
                        , orientationZ     :: Orientation
                        } -> Lattice a

latticeConstructor :: Num a => (a -> C.Lattice a) -> a -> (Int,Int,Int)
                                -> (Int,Int,Int) -> (Int,Int,Int) -> Lattice a
latticeConstructor l lC (xi,xj,xk) (yi,yj,yk) (zi,zj,zk) = Lattice (l lC)
    (Orient 'x' xi xj xk) (Orient 'y' yi yj yk) (Orient 'z' zi zj zk)

none::Num a => a -> (Int,Int,Int) -> (Int,Int,Int) -> (Int,Int,Int) -> Lattice a
none = latticeConstructor None

bcc ::Num a => a -> (Int,Int,Int) -> (Int,Int,Int) -> (Int,Int,Int) -> Lattice a
bcc = latticeConstructor BCC

fcc ::Num a => a -> (Int,Int,Int) -> (Int,Int,Int) -> (Int,Int,Int) -> Lattice a
fcc = latticeConstructor FCC

sc  ::Num a => a -> (Int,Int,Int) -> (Int,Int,Int) -> (Int,Int,Int) -> Lattice a
sc  = latticeConstructor SC

instance (Eq a) => Eq (Lattice a) where
    Lattice lS oX oY oZ==Lattice lS' oX' oY' oZ' = lS==lS'
                                                && oX==oX' && oY==oY' && oZ==oZ'

instance (Show a) => Show (Lattice a) where
    show (Lattice lC oX oY oZ) = unwords [show lC,show oX,show oY,show oZ]

latticeConstant :: Lattice a -> a
latticeConstant = C.latticeConstant . latticeStructure

isLatticeType :: (Eq a,Num a) => Lattice a -> (a -> C.Lattice a) -> Bool
isLatticeType l lt = l==l`toLatticeType`lt

toLatticeType :: Num a => Lattice a -> (a -> C.Lattice a) -> Lattice a
toLatticeType l lt = latticeConstructor lt (latticeConstant l)
                                                    (toTuple orientationX)
                                                    (toTuple orientationY)
                                                    (toTuple orientationZ)
    where toTuple o = (\(Orient _ x y z) -> (x,y,z)) $ o l :: (Int,Int,Int)


lattice :: (Show a) => Lattice a -> LAMMPS ()
lattice l = command "lattice" [show l]


minRegionDirValue :: Orientation -> Plane -> Region -> Bound
minRegionDirValue o p r = -maxRegionDirValue (invertOrientation o) p r

maxRegionDirValue :: Orientation -> Plane -> Region -> Bound
maxRegionDirValue o p (C.Union _ rs  ) = maximum $ maxRegionDirValue o p`map`rs
maxRegionDirValue o p (C.Block _ xlo xhi ylo yhi zlo zhi)
    | d<|>d'/=0 || d<|>d''/=0 || d'<|>d''/=0 = error $ "non-orthogonal crystal"
                                                     ++" orientations not "
                                                     ++"implemented"
    | otherwise                              = sign*len
    where
        d        = orientationDir o
        (d',d'') = vectors p
        n        = normal  p
        p0       = point   p

        sign   = signum((corner-p0)<|>d)
        len    = abs(((p0-corner)<|>n)/(d<|>n))
        corner = ( if fst3 d>=0 then xhi else xlo
                 , if snd3 d>=0 then yhi else ylo
                 , if thd3 d>=0 then zhi else zlo)

inRegion :: Coordinate -> Region -> Bool
inRegion (x,y,z) (Block _ xlo xhi ylo yhi zlo zhi) = x>=xlo&&x<=xhi
                                                   &&y>=ylo&&y<=yhi
                                                   &&z>=zlo&&z<=zhi
inRegion c       (Union _ rs                     ) = any (c`inRegion`) rs


gridInRegion :: Region -> Coordinate -> Orientation -> Bound
                                     -> Orientation -> Bound
                                     -> Orientation -> Bound -> [Coordinate]
gridInRegion r xyz0 oi di oj dj ok dk = filter (`inRegion`r)
                                            [ xyzi+xyzj+xyzk
                                            | xyzi <- grid1D oi oj ok di
                                            , xyzj <- grid1D oj ok oi dj
                                            , xyzk <- grid1D ok oi oj dk
                                            ]
   where
        grid1D :: Orientation -> Orientation -> Orientation -> Bound
                    -> [(Bound,Bound,Bound)]
        grid1D o o' o'' delta = map (((`cmap`oDir) . (*)) . (+d0) . (*delta))
                                    [(dMin-d0)/delta..(dMax-d0)/delta]

            where
                p = Plane 0 (orientationDir o') (orientationDir o'')
                oDir = orientationDir o
                d0   = oDir<|>xyz0
                [dMin,dMax] = sort [ minRegionDirValue o p r
                                   , maxRegionDirValue o p r]
