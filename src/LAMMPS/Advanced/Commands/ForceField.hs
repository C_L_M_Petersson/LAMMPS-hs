module LAMMPS.Advanced.Commands.ForceField
(   module LAMMPS.Core.Commands.ForceField

,   setupReaxFF
,   reaxFFQEQFix
) where

import           Data.Char
import           Data.List

import           LAMMPS.Advanced.Commands
import           LAMMPS.Advanced.Commands.Atom
import           LAMMPS.Advanced.Commands.Atom.Elements
import           LAMMPS.Advanced.Commands.Fix
import           LAMMPS.Advanced.Commands.Fix.Styles    as FS
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.ForceField
import           LAMMPS.Core.Commands.ForceField.Styles as FFS


setupReaxFF :: FilePath -> [Atom] -> FilePath -> [Argument] -> LAMMPS()
setupReaxFF fpFF atoms fpC args = pairStyle [FFS.Reax,FFS.C] (fpC:args)
    >> pairCoeff Wildcard Wildcard (fpFF:map (show . getElement) atoms)

reaxFFQEQFix :: Group -> Int -> Double -> Double -> Double -> Fix
reaxFFQEQFix g n cutlo cuthi tol = Fix g [FS.QEQ,FS.Reax]
    (show n:map show [cutlo,cuthi,tol]++["reax/c"])
