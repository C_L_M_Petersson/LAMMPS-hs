module LAMMPS.Advanced.State
(   module LAMMPS.Core.State

,   LAMMPS
,   withLAMMPS

,   addDump
,   remDump
,   getDumpID

,   addFix
,   remFix
,   getFixID

,   getRandomR
,   getRandom
,   getRandomRs
,   getRandoms
) where

import           Control.Monad.State

import           Data.Functor

import           LAMMPS.Advanced.Utils
import           LAMMPS.Core.Commands.Dump
import           LAMMPS.Core.Commands.Fix
import           LAMMPS.Core.State         hiding (LAMMPS, openLAMMPS,
                                            withLAMMPS)
import qualified LAMMPS.Core.State         as Core

import           System.Random             hiding (getStdGen)


data Advanced = Advanced
                { gen    :: StdGen
                , dumps  :: [Dump]
                , fixes  :: [Fix]
                , nCPUs' :: Int
                , rank'  :: Int
                , pipe'  :: LAMMPSPipe
                }

instance LAMMPSState Advanced where
    nCPUs = nCPUs'
    rank  = rank'
    pipe  = pipe'


type LAMMPS a = Core.LAMMPS Advanced a


withLAMMPS :: LAMMPSContext -> LAMMPS a -> IO()
withLAMMPS = Core.withLAMMPS
           $ Advanced
                 (mkStdGen 0)
                 [] []


instance Eq Dump where
    Dump _ g ds _ as==Dump _ g' ds' _ as' = g==g' && ds==ds' && as==as'

addDump :: (ID -> Dump) -> LAMMPS Dump
addDump d = gets ((`addDump'`1) . dumps) >>= \d' ->
                modify (\a -> a{ dumps = d':dumps a }) >> return d'
    where
        addDump' :: [Dump] -> Int -> Dump
        addDump' ds i = if show i`notElem`map getID ds
                            then d (show i) else addDump' ds (i+1)

remDump :: (ID -> Dump) -> LAMMPS ID
remDump d = gets dumps >>= \ds ->
    modify (\a -> a{ dumps = filter (/=d"") (dumps a) } )
        >> return (getID . head' "Dump does not exist." $ filter (==d"") ds)

getDumpID :: (ID -> Dump) -> LAMMPS ID
getDumpID d = gets dumps
    <&> (getID . head' "Dump does not exist" . filter (==d""))


instance Eq Fix where
    Fix _ g fss as==Fix _ g' fss' as' = g==g' && fss==fss' && as==as'

addFix :: (ID -> Fix) -> LAMMPS Fix
addFix f = gets ((`addFix'`1) . fixes) >>= \f' ->
                modify (\a -> a{ fixes = f':fixes a }) >> return f'
    where
        addFix' :: [Fix] -> Int -> Fix
        addFix' fs i = if show i`notElem`map getID fs
                           then f (show i) else addFix' fs (i+1)

remFix :: (ID -> Fix) -> LAMMPS ID
remFix f = gets fixes >>= \fs ->
    modify (\a -> a{ fixes = filter (/=f"") (fixes a) } )
        >> return (getID . head' "Fix does not exist" $ filter (==f"") fs)

getFixID :: (ID -> Fix) -> LAMMPS ID
getFixID f = gets fixes
    <&> (getID . head' "Fix does not exist" . filter (==f""))


getStdGen :: LAMMPS StdGen
getStdGen = gets (split . gen) >>= \(g,g') ->
    modify (\a -> a{ gen = g }) >> return g'

getRandomR :: Random a => (a,a) -> LAMMPS a
getRandomR x = fst . randomR x <$> getStdGen

getRandom :: Random a => LAMMPS a
getRandom = fst . random <$> getStdGen

getRandomRs :: Random a => (a,a) -> LAMMPS [a]
getRandomRs x = randomRs x <$> getStdGen

getRandoms :: Random a => LAMMPS [a]
getRandoms = randoms <$> getStdGen
