module LAMMPS.Basic.State
(   module LAMMPS.Core.State

,   LAMMPS
,   withLAMMPS
) where

import           LAMMPS.Core.State hiding (LAMMPS, openLAMMPS, withLAMMPS)
import qualified LAMMPS.Core.State as C



data Basic = Basic
             { nCPUs' :: Int
             , rank'  :: Int
             , pipe'  :: LAMMPSPipe
             }

instance LAMMPSState Basic where
    nCPUs = nCPUs'
    rank  = rank'
    pipe  = pipe'


type LAMMPS a = C.LAMMPS Basic a


withLAMMPS :: LAMMPSContext -> LAMMPS a -> IO()
withLAMMPS = C.withLAMMPS Basic
