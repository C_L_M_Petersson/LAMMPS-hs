module LAMMPS.Basic.Commands.Atom
(   module LAMMPS.Core.Commands.Atom
,   Atom(..)

,   createRegionAtoms
,   createRandomAtoms
,   createSingleAtom
) where

import           Control.Monad.IO.Class

import           Data.Maybe

import           LAMMPS.Basic.State
import           LAMMPS.Basic.Utils
import           LAMMPS.Core.Commands.Atom hiding (createRegionAtoms,
                                            createRandomAtoms,
                                            createSingleAtom)
import qualified LAMMPS.Core.Commands.Atom as Core
import           LAMMPS.Core.Commands.Box


newtype Atom = Atom
               { atomID :: ID
               }
    deriving(Eq,Show)

instance LAMMPSObject Atom where
    getID = atomID


createRegionAtoms :: Atom -> Region -> LAMMPS()
createRegionAtoms = Core.createRegionAtoms

createRandomAtoms :: Atom -> Region -> Int -> Seed -> LAMMPS()
createRandomAtoms = Core.createRandomAtoms

createSingleAtom :: Atom -> Bound -> Bound -> Bound -> LAMMPS()
createSingleAtom = Core.createSingleAtom
