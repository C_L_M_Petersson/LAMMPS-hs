module LAMMPS.Basic.Commands.Dump
(   module LAMMPS.Core.Commands.Dump

,   dump
) where

import           Data.Foldable

import           LAMMPS.Basic.State
import           LAMMPS.Core.Commands.Dump hiding (dump)
import qualified LAMMPS.Core.Commands.Dump as Core


dump :: Dump -> Int -> [DumpModification] -> LAMMPS Dump
dump d n dms = Core.dump d n >> foldlM dumpModify d dms
