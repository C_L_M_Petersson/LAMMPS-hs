module Examples
(   Interface(..)
,   Example  (..)

,   runAdvancedExample
,   runBasicExample
) where

import           Examples.Advanced     as AE
import           Examples.Basic        as BE

import           LAMMPS.Advanced.State as AS
import           LAMMPS.Basic.State    as BS


data Interface = Core | Basic | Advanced
    deriving(Show)

data Example = Molecule | NanoWire | Oxidation | LowPressure
    deriving(Show)


runAdvancedExample :: LAMMPSContext -> Example -> IO()
runAdvancedExample lc Molecule    = AS.withLAMMPS lc AE.lmpMolecule
runAdvancedExample lc Oxidation   = AS.withLAMMPS lc AE.lmpOxidation
runAdvancedExample lc LowPressure = AS.withLAMMPS lc AE.lmpLowPressureOxidation
runAdvancedExample _  e           = exampleDoesNotExistError e "Advanced"

runBasicExample :: LAMMPSContext -> Example -> IO()
runBasicExample lc NanoWire = BS.withLAMMPS lc BE.lmpNanoWire
runBasicExample _  e        = exampleDoesNotExistError e "Basic"


exampleDoesNotExistError :: Example -> String -> a
exampleDoesNotExistError e i = error $ "Example "++show e++" has not been "
                                     ++"prepared for use with the "++i++
                                     " interface."
