module Tools.Advanced.Query
(   getDumpNAtoms
,   getProcessedDumpAtomInfo
,   getDumpAtomInfo
) where

import           Data.Composition

import           LAMMPS.Advanced.Commands.Atom
import           LAMMPS.Advanced.Commands.Dump
import           LAMMPS.Advanced.Commands.Dump.Styles
import           LAMMPS.Advanced.State
import           LAMMPS.Advanced.Utils
import qualified LAMMPS.Core.Commands.Dump                  as CD

import qualified Tools.Core.Query                           as C


getDumpNAtoms :: Group -> LAMMPS Int
getDumpNAtoms = flip (withAdvancedDump C.getDumpNAtoms) ["id"]

getProcessedDumpAtomInfo :: ([Bound] -> a) -> Group -> [Argument] -> LAMMPS [a]
getProcessedDumpAtomInfo f = (map f<$>).:getDumpAtomInfo

getDumpAtomInfo :: Group -> [Argument] -> LAMMPS [[Bound]]
getDumpAtomInfo = withAdvancedDump C.getDumpAtomInfo


withAdvancedDump :: (CD.Dump -> a) -> Group -> [Argument] -> a
withAdvancedDump f g cs = f $ toCoreDump (Dump g [Custom] cs) "" "tmpDump"
