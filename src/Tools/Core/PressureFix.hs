{-# LANGUAGE ScopedTypeVariables #-}
module Tools.Core.PressureFix
(   placeMols
,   sortXYZsBySpace
) where
import System.IO.Unsafe
import System.Random

import           Data.Composition
import           Data.List
import           Data.Tuple.Extra

import           LAMMPS.Core.Commands.Atom
import           LAMMPS.Core.Commands.Box
import           LAMMPS.Core.Commands.Dump
import           LAMMPS.Core.Commands.Dump.Styles
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils

import           Tools.Core.Query


placeMols :: forall s. LAMMPSState s => Bound -> (Coordinate -> LAMMPS s ())
                -> ID -> Int -> [Coordinate] -> Group -> LAMMPS s Region
placeMols cutOff place id n cs gNearby = (Union id<$>) $
        getProcessedDumpAtomInfo (\[x,y,z] -> (x,y,z))
            (Dump "tmp" gNearby [Custom] "" ["x","y","z"])
                >>=placeMolsRec n cs
    where
        placeMolsRec :: Int -> [Coordinate] -> [Coordinate] -> LAMMPS s [Region]
        placeMolsRec _ []     _        = return []
        placeMolsRec n (c:cs) csNeraby
            | n<1                      = return []
            | not $ null nearby        = placeMolsRec n cs csNeraby
            | otherwise                = place c>>
                (:)<$>region (uncurry3 (Sphere (id++show n)) c cutOff)
                   <*>placeMolsRec (n-1) cs (c:csNeraby)
            where nearby = filter ((<cutOff**2) . norm2 . (c-)) csNeraby


sortXYZsBySpace :: LAMMPSState s => Group -> [Coordinate]
                                                        -> LAMMPS s [Coordinate]
sortXYZsBySpace gNearby cs = (\csNearby -> sortXYZsRec csNearby
                                            $ map (initialWeight csNearby) cs)
        <$>getProcessedDumpAtomInfo (\[x,y,z] -> (x,y,z))
            (Dump "tmp" gNearby [Custom] "" ["x","y","z"])
    where
        sortXYZsRec :: [Coordinate] -> [(Coordinate,Bound)] -> [Coordinate]
        sortXYZsRec csNearby []  = []
        sortXYZsRec csNearby cws = c : sortXYZsRec (c:csNearby)
                                                   (map (updateWeight c) cws')
            where ((c,_):cws') = sortOn snd cws

        updateWeight :: Coordinate -> (Coordinate,Bound) -> (Coordinate,Bound)
        updateWeight c (c',w) = (c',w+weight c c')

        initialWeight :: [Coordinate] -> Coordinate -> (Coordinate,Bound)
        initialWeight cs c = (c,sum $ map (weight c) cs)

        weight :: Coordinate -> Coordinate -> Bound
        weight c c' = 1/norm2(c-c')
