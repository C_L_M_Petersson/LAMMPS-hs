module Tools.Core.Query
(   getDumpNAtoms
,   getProcessedDumpAtomInfo
,   getDumpAtomInfo
) where

import           Control.Monad.Extra
import           Control.Parallel.MPI.Simple

import           LAMMPS.Core.Commands.Dump
import           LAMMPS.Core.State
import           LAMMPS.Core.Utils

import           System.Directory
import           System.Posix.Process


getDumpNAtoms :: LAMMPSState s => Dump -> LAMMPS s Int
getDumpNAtoms = (fst<$>) . getDumpInfo

getProcessedDumpAtomInfo :: LAMMPSState s => ([Bound] -> a) -> Dump
                                                                -> LAMMPS s [a]
getProcessedDumpAtomInfo f = (map f<$>) . getDumpAtomInfo

getDumpAtomInfo :: LAMMPSState s => Dump -> LAMMPS s [[Bound]]
getDumpAtomInfo = (snd<$>) . getDumpInfo

getDumpInfo :: LAMMPSState s => Dump -> LAMMPS s (Int,[[Bound]])
getDumpInfo (Dump lID g ds _ as) = do
    fp <- liftIO fpTmpDump
    writeDump $ Dump lID g ds fp as

    ls <- ifM (notM isRoot)
        ( liftIO $ scatterRecv commWorld 0 )
        . liftIO $ do
            ls <- lines<$>readFile fp
            commSize commWorld>>=
                scatterSend commWorld 0 . (`replicate`ls)

    whenM isRoot . liftIO $ removeFile fp

    return (read (ls!!3),map (map read . words) $ drop 9 ls)


fpTmpDump :: IO FilePath
fpTmpDump = ("/tmp/dump."++) . (++".tmp") . show<$>getProcessID
