import           LAMMPS.Core.State (LAMMPSContext (..))

import           Examples


main :: IO()
main = runExample context lmp
    where
        runExample = runBasicExample
        --runExample = runAdvancedExample

        --context   = Echo
        --context   = Execute
        --context   = Write "in.lmp"
        context   = MultipleContexts [Echo,Execute]

        lmp       = NanoWire
        --lmp       = Molecule
        --lmp       = Oxidation
        --lmp       = LowPressure
