export OMP_NUM_THREADS=2

rm out.lmp
rm log.lammps
rm dump.all.*

mpirun -np 16 ./run.x
#mpirun -np 16 /usr/bin/lmp -in in.lmp

#echo -e "\n\n"
#echo "cat log.lammps:"
#cat log.lammps
